package com.johnstotko.baldbudgetV2;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import java.util.ArrayList;


class Budget{

    private Context context;

    private int maxTransactions = 20;

    private Boolean showNotification = false;
    void setShowNotification(Boolean cond){
        this.showNotification = cond;
    }
    Boolean getShowNotification(){
        return this.showNotification;
    }

    private int colorOption = 0;
    int getColorOption(){
        // this is used for the save file, use getColor to retrieve the color
        return colorOption;
    }
    int getColor(){
        int color = 0;
        switch (colorOption){
            case 0:
                // Red
                color = ContextCompat.getColor(context,R.color.red);

                break;
            case 1:
                // Orange
                color = ContextCompat.getColor(context,R.color.orange);

                break;

            case 2:
                // Green
                color = ContextCompat.getColor(context,R.color.green);

                break;

            case 3:
                // Blue
                color = ContextCompat.getColor(context,R.color.blue);
                break;

            case 4:
                // Purple
                color = ContextCompat.getColor(context,R.color.purple);
                break;
        }
        return color;
    }
    int getColorDark(){
        int color = 0;
        switch (colorOption){
            case 0:
                // Red
                color = ContextCompat.getColor(context,R.color.red_dark);

                break;
            case 1:
                // Orange
                color = ContextCompat.getColor(context,R.color.orange_dark);

                break;

            case 2:
                // Green
                color = ContextCompat.getColor(context,R.color.green_dark);

                break;

            case 3:
                // Blue
                color = ContextCompat.getColor(context,R.color.blue_dark);
                break;

            case 4:
                // Purple
                color = ContextCompat.getColor(context,R.color.purple_dark);
                break;
        }
        return color;
    }

    void setColorOption(int option){
        colorOption = option;
    }

    // Name that will be displayed on label
    private String name = "Budget Name";
    void setName(String Name) {
        name = Name;
    }
    String getName() {
        return name;
    }

    // Amount left in budget
    private int amount = 0;
    void setAmount(int Amount) {
        amount = Amount;
    }
    int getAmount() {
        // Only used for saving.
        return amount;
    }
    String getAmountString() {
        // Outputs a string formatted as "$ 10.00"
        int am = amount;
        for(Transaction transaction:transactions){
            if(transaction.getExpense()) {
                am -= transaction.getAmount();
            } else {
                am += transaction.getAmount();
            }
        }
        // Make sure it's positive
        String cents;
        if(am%100 < 0){
            cents = Integer.toString(-am%100);
        } else {
            cents = Integer.toString(am%100);
        }
        // Make sure cents has two digits
        if (cents.length() == 1) {
            cents = "0" + cents;
        }

        if (am<0){
            return "($ " + Integer.toString(-am/100) + "." + cents + ")";
        } else {
            return "$ " + Integer.toString(am/100)+ "." + cents;
        }
    }

    // Renewal Amount
    private String renewAmount = "No renewal"; // default value
    void setRenewAmount(String renewAmount){
        if(renewAmount.trim().length() == 0){
            this.renewAmount = "No renewal";

        } else {
            this.renewAmount = renewAmount;
        }
    }
    String getRenewAmount(){
        return renewAmount;
    }

    // Renewal Frequency
    private String renewFrequency = ""; // default value
    void setRenewFrequency(String frequency){
        if(frequency.trim().length() == 0){
            this.renewFrequency = "";
        } else {
            this.renewFrequency = frequency;
        }
    }
    String getRenewFrequency(){return renewFrequency;}

    // Comment on budget
    private String comment = " ";
    void setComment(String comment){
        this.comment = comment;
    }
    String getComment(){
        return comment;
    }

    // Transaction List
    private ArrayList<Transaction> transactions = new ArrayList<>();
    ArrayList<Transaction> getTransactions(){
        return transactions;
    }
    void addTransaction(Transaction transaction){
        transactions.add(transaction);

        if(transactions.size() > maxTransactions){
            amount+= transactions.get(0).getAmount();
            transactions.remove(0);
        }
    }

    // Constructor
    Budget(Context context) {
        this.context = context;
        maxTransactions = App.getMaxTransactionsPerBudget();
    }
}
