package com.johnstotko.baldbudgetV2;

/**
 * Created by John on 7/2/2017.
 */

public class Transaction {


    private String date; // format: MM/DD
    private String description; // e.g. Morning Coffee
    private int amount; // in cents ($1.00 == 100)
    private boolean expense; // if true, transaction is an expense, otherwise it's income

    public Transaction(String date, String description, int amount, boolean expense){
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.expense = expense;

    }

    public void setDate(String date){
        this.date = date;
    }
    public String getDate(){
        return this.date;
    }

    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return this.description;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }
    public int getAmount(){
        return this.amount;
    }
    public String getAmountString(){

        String cents = Integer.toString(amount%100);
        if(cents.length() == 1){
            cents = "0" + cents;
        }

        if (expense){
            return "($ " + Integer.toString(amount/100) + "." + cents + ")";
        } else {
            return "$ " + Integer.toString(amount/100) + "." + cents;
        }
    }

    public void setExpense(boolean expense){
        this.expense = expense;
    }
    public boolean getExpense(){
        return this.expense;
    }
}
