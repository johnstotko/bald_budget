package com.johnstotko.baldbudgetV2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;

/**
 * https://developer.android.com/guide/topics/ui/notifiers/notifications.html
 */
class NotificationHelper {

    private Notification.Builder builder;
    private NotificationManager notificationManager;

    NotificationHelper(Context context){
        builder = new Notification.Builder(context);

        notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        builder.setSmallIcon(R.drawable.notification_icon);

        Intent resultIntent = new Intent(context,MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        builder.setContentIntent(resultPendingIntent);
        builder.setOngoing(true);
    }


    void pushNotification(int ID, String title, String detail, int color){

        builder.setContentTitle(title);
        builder.setContentText(detail);
        builder.setColor(color);

        notificationManager.notify(ID, builder.build());

    }

    void clearNotifications(){
        notificationManager.cancelAll();
    }





}
