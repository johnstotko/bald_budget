package com.johnstotko.baldbudgetV2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;

class BudgetAdapter extends BaseAdapter {

    private ArrayList<Budget> budgets;
    private LayoutInflater budgetInf;

    BudgetAdapter(Context c, ArrayList<Budget> theBudgets){
        budgets = theBudgets;
        budgetInf = LayoutInflater.from(c);
    }

    @Override
    public int getCount(){

        return budgets.size();
    }

    @Override
    public Object getItem(int arg0){

        return null;
    }

    @Override
    public long getItemId(int arg0){

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //map to budget layout
        // TODO Recycle view
        // https://stackoverflow.com/questions/25381435/unconditional-layout-inflation-from-view-adapter-should-use-view-holder-patter
        RelativeLayout budgetLay = (RelativeLayout)budgetInf.inflate
                (R.layout.budget, parent, false);

        // get name, amount, renew, and icon views
        TextView nameView = (TextView)budgetLay.findViewById(R.id.budget_name);
        TextView amountView = (TextView)budgetLay.findViewById(R.id.budget_amount);
        TextView renewView = (TextView)budgetLay.findViewById(R.id.budget_renew);
        ImageView iconView = (ImageView)budgetLay.findViewById(R.id.budget_icon);

        // get budget using position
        Budget currBudget = budgets.get(position);

        // set views
        nameView.setText(currBudget.getName());
        amountView.setText(currBudget.getAmountString());
        renewView.setText(currBudget.getRenewAmount() + "\n" + currBudget.getRenewFrequency());
        iconView.setColorFilter(currBudget.getColor());

        //set position as tag
        budgetLay.setTag(position);
        return budgetLay;

    }


}
