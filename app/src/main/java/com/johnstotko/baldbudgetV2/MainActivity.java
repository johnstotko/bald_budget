package com.johnstotko.baldbudgetV2;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    App app;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = new App(this);
        app.load();

    }


    @Override
    protected void onResume(){
        super.onResume();

        ListView budgetView = (ListView)findViewById(R.id.budget_list);
        BudgetAdapter budgetAdapter = new BudgetAdapter(this, App.getBudgetList());
        budgetView.setAdapter(budgetAdapter);

    }

    @Override
    protected void onStop(){
        super.onStop();
        app.renewNotifications();

    }

    public void newBudget(View v){

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.add_budget_dialog, null);
        alertBuilder.setView(dialogView);
        final AlertDialog dialog = alertBuilder.create();

        final EditText etName = (EditText) dialogView.findViewById(R.id.et_add_budget_name);
        final EditText etAmount = (EditText) dialogView.findViewById(R.id.et_add_budget_amount);
        Button btnCreate = (Button) dialogView.findViewById(R.id.button_add_budget_create);

        btnCreate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if (    etName.getText().toString().length() != 0 &&
                        etAmount.getText().toString().length() != 0){

                    Budget budget = new Budget(MainActivity.this);
                    budget.setName(etName.getText().toString());
                    budget.setAmount((int)(Double.parseDouble(etAmount.getText().toString()) * 100));
                    App.getBudgetList().add(budget);
                    BudgetAdapter budgetAdapter = new BudgetAdapter(MainActivity.this, App.getBudgetList());
                    ((ListView)findViewById(R.id.budget_list)).setAdapter(budgetAdapter);
                    Toast.makeText(MainActivity.this, "Budget Created", Toast.LENGTH_SHORT).show();
                    app.save();
                    dialog.dismiss();

                } else {
                    Toast.makeText(MainActivity.this, "Please fill all fields", Toast.LENGTH_SHORT).show();
                }


            }
        });

        dialog.show();

    }

    public void viewBudget(View v){

        App.setPOI(Integer.parseInt(v.getTag().toString()));
        startActivity(new Intent(MainActivity.this, BudgetInfoActivity.class));
    }

    public void toInfoActivity(View v){
        startActivity(new Intent(MainActivity.this, InfoActivity.class));
    }
}

