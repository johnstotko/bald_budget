package com.johnstotko.baldbudgetV2;

import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class BudgetInfoActivity extends AppCompatActivity {

    private int POI;
    private ArrayList<Budget> budgetList = new ArrayList<>();
    private Window window;

    ImageButton menuButton;
    ImageView titleBox;
    TextView nameView;
    TextView amountView;
    TextView renewView;
    TextView commentView;

    App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_info);

        overridePendingTransition(R.anim.slide_in, R.anim.do_nothing);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }



        app = new App(this);
        budgetList = App.getBudgetList();
        POI = App.getPOI();

        // Action Bar Icons
            ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
            backButton.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    finish();
                }
            } );

        //Name
            nameView = (TextView)findViewById(R.id.text_name);
            nameView.setText(budgetList.get(POI).getName());
        // Colored box behind nameView
            titleBox = (ImageView)findViewById(R.id.title_bar);
            titleBox.setColorFilter(budgetList.get(POI).getColor());
        // Set the window color
            window.setStatusBarColor(budgetList.get(POI).getColorDark());

        // Amount text view
            amountView = (TextView)findViewById(R.id.amount);

        // Renewal text view
            renewView = (TextView)findViewById(R.id.renew_amount);
            renewView.setText(R.string.coming_soon);
        // Comment text view
            commentView = (TextView)findViewById(R.id.comment);
            commentView.setText(budgetList.get(POI).getComment());

        // The menu Button
            menuButton = (ImageButton)findViewById(R.id.menu_button);
            menuButton.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    createPopUpMenu();
                }
            });
    }

    @Override
    protected void onResume(){
        super.onResume();

        ListView transactionView = (ListView)findViewById(R.id.transaction_list);
        TransactionAdapter transactionAdapter = new TransactionAdapter(this, budgetList.get(POI).getTransactions());
        transactionView.setAdapter(transactionAdapter);

        amountView.setText(budgetList.get(POI).getAmountString());
    }

    protected
    void onPause(){
        super.onPause();
        overridePendingTransition(R.anim.do_nothing, R.anim.slide_out);
    }

    public void createPopUpMenu(){
        // https://www.javatpoint.com/android-popup-menu-example

        PopupMenu popupMenu = new PopupMenu(BudgetInfoActivity.this,menuButton);

        popupMenu.getMenuInflater().inflate(R.menu.pop_up_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
            public boolean onMenuItemClick(MenuItem item){

                switch(item.getTitle().toString()){
                    case "Delete":
                        deleteBudget();
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void deleteBudget(){
        Toast.makeText(BudgetInfoActivity.this,
                budgetList.get(POI).getName() + " has been deleted.",
                Toast.LENGTH_SHORT).show();

        budgetList.remove(POI);
        App.setBudgetList(budgetList);
        app.renewNotifications();
        finish();
    }

    public void editBudget(View v){
        // Changes will be applied on dismissal of dialog

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.budget_edit_dialog, null);
        alertBuilder.setView(dialogView);
        final AlertDialog dialog = alertBuilder.create();


        // Color will be changed as it is chosen
        // Doing an array makes it possible not to have 5 separate onClickListeners
        final ImageButton[] colorButtons = new ImageButton[5];
        colorButtons[0] = (ImageButton) dialogView.findViewById(R.id.red_button);
        colorButtons[1] = (ImageButton) dialogView.findViewById(R.id.orange_button);
        colorButtons[2] = (ImageButton) dialogView.findViewById(R.id.green_button);
        colorButtons[3] = (ImageButton) dialogView.findViewById(R.id.blue_button);
        colorButtons[4] = (ImageButton) dialogView.findViewById(R.id.purple_button);

        // Load the checks
        final ImageView[] checkViews = new ImageView[5];
        checkViews[0] = (ImageView) dialogView.findViewById(R.id.red_check);
        checkViews[1] = (ImageView) dialogView.findViewById(R.id.orange_check);
        checkViews[2] = (ImageView) dialogView.findViewById(R.id.green_check);
        checkViews[3] = (ImageView) dialogView.findViewById(R.id.blue_check);
        checkViews[4] = (ImageView) dialogView.findViewById(R.id.purple_check);

        // Put check over the button that is selected
        for (int i = 0; i < checkViews.length; i++){
            if(budgetList.get(POI).getColorOption() != i){
                checkViews[i].setVisibility(View.INVISIBLE);
            } else {
                checkViews[i].setVisibility(View.VISIBLE);
            }
        }

        for(int i = 0; i<colorButtons.length; i++){
            final int finI = i; // final copy for onClick()
            colorButtons[i].setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    App.getBudgetList().get(POI).setColorOption(finI);
                    titleBox.setColorFilter(budgetList.get(POI).getColor());
                    window.setStatusBarColor(budgetList.get(POI).getColorDark());

                    // Put check over the button that is selected
                    for (int i = 0; i < checkViews.length; i++){
                        if(budgetList.get(POI).getColorOption() != i){
                            checkViews[i].setVisibility(View.INVISIBLE);
                        } else {
                            checkViews[i].setVisibility(View.VISIBLE);
                        }
                    }

                }
            });

        }

        final EditText etName = (EditText) dialogView.findViewById(R.id.et_edit_budget_name);
        etName.setText(App.getBudgetList().get(POI).getName());

        final EditText etComment = (EditText) dialogView.findViewById(R.id.et_edit_budget_comment);
        etComment.setText(App.getBudgetList().get(POI).getComment());

        final Switch swNotification = (Switch) dialogView.findViewById(R.id.show_notification);
        swNotification.setChecked(App.getBudgetList().get(POI).getShowNotification());

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(etName.getText().toString().length() != 0){
                    App.getBudgetList().get(POI).setName
                            (etName.getText().toString());
                } else {
                    App.getBudgetList().get(POI).setName(" ");
                }

                if(etComment.getText().toString().length() != 0){
                    App.getBudgetList().get(POI).setComment
                            (etComment.getText().toString());
                } else {
                    App.getBudgetList().get(POI).setComment(" ");
                }

                App.getBudgetList().get(POI).setShowNotification(swNotification.isChecked());

                app.save(); // Transactions will be changed,
                            // in accordance to name change
                app.renewNotifications();
                nameView.setText(budgetList.get(POI).getName());
                commentView.setText(budgetList.get(POI).getComment());
            }
        });

        dialog.show();

    }

    public void addTransaction(View v){

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.add_transaction_dialog, null);
        alertBuilder.setView(dialogView);
        final AlertDialog dialog = alertBuilder.create();

        final EditText etDescription = (EditText) dialogView.findViewById(R.id.et_add_transaction_name);
        final EditText etAmount = (EditText) dialogView.findViewById(R.id.et_add_transaction_amount);
        final Switch swInOut = (Switch) dialogView.findViewById(R.id.sw_in_out);
        Button btnCreate = (Button) dialogView.findViewById(R.id.button_add_transaction_create);
        Button btnCancel = (Button) dialogView.findViewById(R.id.button_add_transaction_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                dialog.dismiss();

            }
        });


        btnCreate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                if(     etDescription.getText().length() != 0 &&
                        etAmount.getText().length() != 0 ){

                    String description = etDescription.getText().toString();
                    int amount = ((int)(Double.parseDouble(etAmount.getText().toString()) * 100));

                    Calendar c = Calendar.getInstance();
                    String date = String.valueOf(c.get(Calendar.MONTH) + 1)
                            + "/"
                            + String.valueOf(c.get(Calendar.DAY_OF_MONTH));

                    boolean expense = !swInOut.isChecked(); // If it is checked it is income

                    budgetList.get(POI).addTransaction(new Transaction(date, description, amount, expense));
                    ((ListView)findViewById(R.id.transaction_list)).setAdapter(
                            new TransactionAdapter(BudgetInfoActivity.this, budgetList.get(POI).getTransactions())
                    );


                    amountView.setText(budgetList.get(POI).getAmountString());
                    app.save();
                    app.renewNotifications();
                    dialog.dismiss();

                } else {
                    Toast.makeText(BudgetInfoActivity.this, "Please fill all fields", Toast.LENGTH_SHORT).show();
                }



            }
        });

        dialog.show();

    }


}
