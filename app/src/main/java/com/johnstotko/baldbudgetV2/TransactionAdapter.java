package com.johnstotko.baldbudgetV2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;


class TransactionAdapter extends BaseAdapter {

    private ArrayList<Transaction> transactions;
    private LayoutInflater transactionInf;

    TransactionAdapter(Context c, ArrayList<Transaction> theTransactions){
        transactions = theTransactions;
        transactionInf = LayoutInflater.from(c);
    }

    @Override
    public int getCount(){

        return transactions.size();
    }

    @Override
    public Object getItem(int arg0){

        return null;
    }

    @Override
    public long getItemId(int arg0){

        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        // map to transaction layout
        // TODO Recycle View
        // https://stackoverflow.com/questions/25381435/unconditional-layout-inflation-from-view-adapter-should-use-view-holder-patter
        RelativeLayout transactionLay = (RelativeLayout)transactionInf.inflate
                (R.layout.transaction, parent, false);

        // Get description, amount, date views
        TextView descView = (TextView)transactionLay.findViewById(R.id.trans_description);
        TextView amountView = (TextView)transactionLay.findViewById(R.id.trans_amount);
        TextView dateView = (TextView)transactionLay.findViewById(R.id.trans_date);

        // get transaction using position
        Transaction currTransaction = transactions.get(position);

        // Set views
        descView.setText(currTransaction.getDescription());
        amountView.setText(currTransaction.getAmountString());
        dateView.setText(currTransaction.getDate());

        // set position as tag
        transactionLay.setTag(position);
        return transactionLay;

    }

}
