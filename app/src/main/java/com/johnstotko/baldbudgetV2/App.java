package com.johnstotko.baldbudgetV2;

import android.content.Context;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class App {
    /** Purpose of App Class
     *
     *  - Manage notifications
     *  - Application state
     *      - List of budgets
     *      - Position of Interest
      */
    private static final String DEBUG_TAG = "App";
    private static final String budgetFile = "budgetsSaveFile"; // Used for saving.loading budgets
    private static final String transactionFile = "transactionFile"; // Used for saving/loading transactions
    private static final String splitCharacter = "%%"; // splitting character for save/load
    private Context context;

    private static ArrayList<Budget> budgetList = new ArrayList<>();
    private static int POI; // Used to grab a particular index of budgetList across activities
    private static int MAX_TRANSACTIONS_PER_BUDGET = 10;

    static void setMaxTransactionsPerBudget(int max){
        MAX_TRANSACTIONS_PER_BUDGET = max;
    }
    static int getMaxTransactionsPerBudget(){
        return MAX_TRANSACTIONS_PER_BUDGET;
    }

    static void setBudgetList(ArrayList<Budget> BudgetList){
        budgetList = BudgetList;
    }
    static ArrayList<Budget> getBudgetList(){
        return budgetList;
    }

    static int getPOI(){
        return POI;
    }
    static void setPOI(int position){
        POI = position;
    }

    void renewNotifications(){
        NotificationHelper notificationHelper = new NotificationHelper(context);
        notificationHelper.clearNotifications();
        int i = 0;
        for (Budget budget: budgetList){
            if (budget.getShowNotification()){
                notificationHelper.pushNotification(i,
                                                    budget.getName(),
                                                    budget.getAmountString(),
                                                    budget.getColor());
                i++;
            }
        }
    }

    void load(){

        budgetList = new ArrayList<Budget>();

        try {
            ArrayList<String> rawBudgets = getStringFromFile(budgetFile);
            for (String line : rawBudgets){
                String [] parts = line.split(Pattern.quote(splitCharacter));

                Budget budget = new Budget(context);

                budget.setName(parts[0]);
                budget.setAmount(Integer.parseInt(parts[1]));
                budget.setRenewAmount(parts[2]);
                budget.setRenewFrequency(parts[3]);
                budget.setComment(parts[4]);
                budget.setColorOption(Integer.parseInt(parts[5]));
                budget.setShowNotification(Boolean.parseBoolean(parts[6]));

                budgetList.add(budget);

            }

        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            ArrayList<String> rawTransactions = getStringFromFile(transactionFile);
            for (String line : rawTransactions){
                String [] parts = line.split(Pattern.quote(splitCharacter));

                String budgetName = parts[0];
                String description = parts[1];
                int amount = Integer.parseInt(parts[2]);
                String date = parts[3];
                boolean expense = Boolean.parseBoolean(parts[4]);

                for (Budget budget: budgetList){
                    if (budgetName.equals(budget.getName())){
                        budget.addTransaction(new Transaction(date, description, amount, expense));
                        break;
                    }
                }



            }

        } catch (Exception e){
            e.printStackTrace();
        }


    }

    void save(){

        // This function writes all necessary data to file.

        String temp;
        FileOutputStream fos;

        // Save Budgets, one per line
        try {
            fos = context.openFileOutput(budgetFile, Context.MODE_PRIVATE);
            for(Budget budget:budgetList) {
                temp = budget.getName() + splitCharacter;
                temp += Integer.toString(budget.getAmount()) + splitCharacter;
                temp += budget.getRenewAmount() + splitCharacter;
                temp += budget.getRenewFrequency() + splitCharacter;
                temp += budget.getComment() + splitCharacter;
                temp += Integer.toString(budget.getColorOption()) + splitCharacter;
                temp += Boolean.toString(budget.getShowNotification());

                temp += "\n";

                try {
                    fos.write(temp.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        // Save Transactions, one per line
        try {
            fos = context.openFileOutput(transactionFile, Context.MODE_PRIVATE);
            for(Budget budget:budgetList) {
                for (Transaction transaction : budget.getTransactions()) {

                    temp = budget.getName() + splitCharacter;
                    temp += transaction.getDescription() + splitCharacter;
                    temp += Integer.toString(transaction.getAmount()) + splitCharacter;
                    temp += transaction.getDate() + splitCharacter;
                    temp += Boolean.toString(transaction.getExpense());
                    temp += "\n";

                    try {
                        fos.write(temp.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private ArrayList<String> getStringFromFile(String filename) throws Exception {
        File file = new File(context.getFilesDir(), filename);
        FileInputStream is = new FileInputStream(file);

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        ArrayList<String> ret = new ArrayList<>();
        String line;

        while((line = reader.readLine()) != null) {
            ret.add(line);
        }

        reader.close();
        return ret;
    }

    App(Context context){
        this.context = context;
    }

}
